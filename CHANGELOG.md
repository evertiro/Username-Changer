# Changelog

## 3.2.2 (25 May 2020)

- Updated: General cleanup
- Updated: Settings library

## 3.2.1

- Updated: Settings library (fixes sysinfo security vulnerability)

## 3.2.0

- Updated: Bring up to VIP standards

## 3.1.1

- Fixed: Issue with multicheck settings fields

## 3.1.0

- Added: Email notification support
- Added: Nickname support
- Improved: SQL performance
- Improved: Dynamically update profile files

## 3.0.1

- Fixed: Typo in readme file

## 3.0.0

- Improved: Switched to an inline profile field (props: Danny van Kooten)
- Added: Settings panel

## 2.1.1

- Removed: Deprecated functions

## 2.0.5

- Fixed: Contributor field

## 2.0.4

- Updated: Plugin metalinks

## 2.0.3

- Fixed: User nice name support

## 2.0.2

- Added: multisite support

## 2.0.1

- Added: Proper username sanitization
- Improved: Minor code cleanup

## 2.0.0

- Improved: Converted to class-based structure

## 1.4

- Added: Include author url (nicename) in update process

## 1.3

- Fixed: Properly escaped vars on POST (I think)

## 1.2

- Fixed: Minor tweak to previous release

## 1.1

- Added: Action on All Users page
- Improved: User dropdown is now sorted alphabetically

## 1.0

- Initial release
