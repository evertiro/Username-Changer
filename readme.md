# Username Changer

## Welcome to our GitLab Repository

Changing usernames _should_ be a pretty straightforward feature. Unfortunately,
WordPress doesn't allow it by default. Username Changer attempts to remedy that
oversight.

### Installation

1. You can clone the GitLab repository:
   `https://gitlab.com/widgitlabs/wordpress/username-changer.git`
2. Or download it directly as a ZIP file:
   `https://gitlab.com/widgitlabs/wordpress/username-changer/-/archive/master/username-changer-master.zip`

This will download the latest developer copy of Username Changer.

### Bugs

If you find an issue, let us know [here](https://gitlab.com/evertiro/username-changer/issues?state=open)!

### Contributions

Anyone is welcome to contribute to Username Changer. Please read the
[guidelines for contributing](https://gitlab.com/evertiro/username-changer/-/blob/master/CONTRIBUTING.md)
to this repository.
