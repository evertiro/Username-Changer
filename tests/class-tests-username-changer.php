<?php
/**
 * Core unit test
 *
 * @package     UsernameChanger\Tests\Core
 * @since       3.0.0
 */

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}


/**
 * Core unit tests
 *
 * @since       3.0.0
 */
class Tests_Username_Changer extends WP_UnitTestCase {


	/**
	 * Test suite object
	 *
	 * @access      protected
	 * @since       3.0.0
	 * @var         object $object The test suite object
	 */
	protected $object;


	/**
	 * Set up this test suite
	 *
	 * @access      public
	 * @since       3.0.0
	 * @return      void
	 */
	public function setUp() {
		parent::setUp();
		$this->object = Username_Changer();
	}


	/**
	 * Tear down this test suite
	 *
	 * @access      public
	 * @since       3.0.0
	 * @return      void
	 */
	public function tearDown() { // phpcs:ignore Generic.CodeAnalysis.UselessOverridingMethod
		parent::tearDown();
	}


	/**
	 * Test the Username_Changer instance
	 *
	 * @access      public
	 * @since       3.0.0
	 * @return      void
	 */
	public function test_username_changer_instance() {
		$this->assertClassHasStaticAttribute( 'instance', 'Username_Changer' );
	}


	/**
	 * Test constants
	 *
	 * @access      public
	 * @since       3.0.0
	 * @return      void
	 */
	public function test_constants() {
		// Make SURE I updated the version.
		$this->assertSame( USERNAME_CHANGER_VER, '3.2.2' );

		// Plugin folder URL.
		$path = str_replace( 'tests/', '', plugin_dir_url( __FILE__ ) );
		$this->assertSame( USERNAME_CHANGER_URL, $path );

		// Plugin folder path.
		$path             = str_replace( 'tests/', '', plugin_dir_path( __FILE__ ) );
		$path             = substr( $path, 0, -1 );
		$username_changer = substr( USERNAME_CHANGER_DIR, 0, -1 );
		$this->assertSame( $username_changer, $path );

		// Plugin root file.
		$path = str_replace( 'tests/', '', plugin_dir_path( __FILE__ ) );
		$this->assertSame( USERNAME_CHANGER_FILE, $path . 'class-username-changer.php' );
	}


	/**
	 * Test includes
	 *
	 * @access      public
	 * @since       3.0.0
	 * @return      void
	 */
	public function test_includes() {
		$this->assertFileExists( USERNAME_CHANGER_DIR . 'class-username-changer.php' );
		$this->assertFileExists( USERNAME_CHANGER_DIR . 'includes/admin/settings/register-settings.php' );
		$this->assertFileExists( USERNAME_CHANGER_DIR . 'includes/admin/actions.php' );

		$this->assertFileExists( USERNAME_CHANGER_DIR . 'includes/class-username-changer-template-tags.php' );
		$this->assertFileExists( USERNAME_CHANGER_DIR . 'includes/misc-functions.php' );
		$this->assertFileExists( USERNAME_CHANGER_DIR . 'includes/scripts.php' );

		/** Check Assets Exist */
		$this->assertFileExists( USERNAME_CHANGER_DIR . 'assets/css/admin.css' );
		$this->assertFileExists( USERNAME_CHANGER_DIR . 'assets/css/admin.min.css' );
		$this->assertFileExists( USERNAME_CHANGER_DIR . 'assets/js/admin.js' );
		$this->assertFileExists( USERNAME_CHANGER_DIR . 'assets/js/admin.min.js' );
		$this->assertFileExists( USERNAME_CHANGER_DIR . 'assets/banner-772x250.jpg' );
		$this->assertFileExists( USERNAME_CHANGER_DIR . 'assets/banner-1544x500.jpg' );
		$this->assertFileExists( USERNAME_CHANGER_DIR . 'assets/icon-128x128.jpg' );
		$this->assertFileExists( USERNAME_CHANGER_DIR . 'assets/icon-256x256.jpg' );
		$this->assertFileExists( USERNAME_CHANGER_DIR . 'assets/icon.svg' );
		$this->assertFileExists( USERNAME_CHANGER_DIR . 'assets/screenshot-1.png' );
		$this->assertFileExists( USERNAME_CHANGER_DIR . 'assets/screenshot-2.gif' );
	}
}
